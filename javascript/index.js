var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var velCar = 0.5;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  console.log("sending = ", json);
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  
  console.log("recieving msgType = ", data.msgType);

  if(data.msgType === 'gameInit') {

    //send({ msgType: "switchLane", data: "Right" });

    //console.log("data.data.race.id = ", data.data.race.id);
    //console.log("data.data.race.track.pieces = ", data.data.race.track.pieces);
  }

  if (data.msgType === 'carPositions') {

    //console.log("data.data[0].id = ", data.data[0].id);data.data.id =  { name: 'Traffic-5', color: 'red' }
    
    //console.log("data.data[0].piecePosition = ", data.data[0].piecePosition);
    //==
    //data.data[0].piecePosition =  { pieceIndex: 3,
    //inPieceDistance: 56.3668309373801,
    //lane: { startLaneIndex: 0, endLaneIndex: 0 },
    //lap: 0 }
    console.log("data.data.length = ", data.data.length);
    for (var i = data.data.length - 1; i >= 0; i--) 
    {
      if(data.data[i].id.name == "SiouxP3")
      {
        //TROCANDO AS LINES SEMPRE PARA PISTA MAIS CURTA DE DENTRO
        if(data.data[i].piecePosition.pieceIndex == 2) {
          send({ msgType: "switchLane", data: "Right" });
        }
        if(data.data[i].piecePosition.pieceIndex == 7) {
          send({ msgType: "switchLane", data: "Left" });
        }
        if(data.data[i].piecePosition.pieceIndex == 17) {
          send({ msgType: "switchLane", data: "Right" });
        }
        if(data.data[i].piecePosition.pieceIndex == 17) {
          send({ msgType: "switchLane", data: "Right" });
        }

        //TROCANDO A VELOCIDADE
        if(data.data[i].piecePosition.pieceIndex == 3) {
          velCar = 0.3;
        }
        if(data.data[i].piecePosition.pieceIndex == 35) {
          velCar = 1;
        }
      }
    };

    send({ msgType: "throttle", data: velCar });
  } else {
  if (data.msgType === 'join') {
    send({
      {msgType: "joinRace", data: {
        botId: {
          name: "keke",
          key: "IVMNERKWEW"
        },
        carCount: 1
      }}
    });
    console.log('Joined = ', data.data);
  } else if (data.msgType === 'gameStart') {
    console.log('Race started');
  } else if (data.msgType === 'gameEnd') {
    console.log('Race ended');
  }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

//keimola

jsonStream.on('error', function() {
  return console.log("disconnected");
});